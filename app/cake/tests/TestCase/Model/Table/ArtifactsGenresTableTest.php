<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ArtifactsGenresTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ArtifactsGenresTable Test Case
 */
class ArtifactsGenresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ArtifactsGenresTable
     */
    public $ArtifactsGenres;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.artifacts_genres',
        'app.artifacts',
        'app.genres'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ArtifactsGenres') ? [] : ['className' => ArtifactsGenresTable::class];
        $this->ArtifactsGenres = TableRegistry::getTableLocator()->get('ArtifactsGenres', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ArtifactsGenres);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
