<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ArtifactsDateReferencedTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ArtifactsDateReferencedTable Test Case
 */
class ArtifactsDateReferencedTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ArtifactsDateReferencedTable
     */
    public $ArtifactsDateReferenced;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.artifacts_date_referenced',
        'app.artifacts',
        'app.rulers',
        'app.months',
        'app.years'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ArtifactsDateReferenced') ? [] : ['className' => ArtifactsDateReferencedTable::class];
        $this->ArtifactsDateReferenced = TableRegistry::getTableLocator()->get('ArtifactsDateReferenced', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ArtifactsDateReferenced);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
