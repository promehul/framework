<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PostingsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PostingsTable Test Case
 */
class PostingsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PostingsTable
     */
    public $Postings;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.postings',
        'app.posting_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Postings') ? [] : ['className' => PostingsTable::class];
        $this->Postings = TableRegistry::getTableLocator()->get('Postings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Postings);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
