<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArtifactsCollection Entity
 *
 * @property int $id
 * @property int|null $artifact_id
 * @property int|null $collection_id
 *
 * @property \App\Model\Entity\Artifact $artifact
 * @property \App\Model\Entity\Collection $collection
 */
class ArtifactsCollection extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_id' => true,
        'collection_id' => true,
        'artifact' => true,
        'collection' => true
    ];
}
