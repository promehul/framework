<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArtifactsDateReferenced Entity
 *
 * @property int $id
 * @property int $artifact_id
 * @property int $ruler_id
 * @property int $month_id
 * @property string $month_no
 * @property int $year_id
 * @property string $day_no
 *
 * @property \App\Model\Entity\Artifact $artifact
 * @property \App\Model\Entity\Ruler $ruler
 * @property \App\Model\Entity\Month $month
 * @property \App\Model\Entity\Year $year
 */
class ArtifactsDateReferenced extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_id' => true,
        'ruler_id' => true,
        'month_id' => true,
        'month_no' => true,
        'year_id' => true,
        'day_no' => true,
        'artifact' => true,
        'ruler' => true,
        'month' => true,
        'year' => true
    ];
}
