<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dynasty[]|\Cake\Collection\CollectionInterface $dynasties
 */
?>
<div class="boxed mx-0">
    <div class="capital-heading"><?= __('Related Actions') ?></div>

        <?= $this->Html->link(__('New Dynasty'), ['action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Proveniences'), ['controller' => 'Proveniences', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Provenience'), ['controller' => 'Proveniences', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Dates'), ['controller' => 'Dates', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Date'), ['controller' => 'Dates', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Rulers'), ['controller' => 'Rulers', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Ruler'), ['controller' => 'Rulers', 'action' => 'add'], ['class' => 'btn-action']) ?>

</div>

<h3 class="display-4 pt-3"><?= __('Dynasties') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('polity') ?></th>
            <th scope="col"><?= $this->Paginator->sort('dynasty') ?></th>
            <th scope="col"><?= $this->Paginator->sort('sequence') ?></th>
            <th scope="col"><?= $this->Paginator->sort('provenience_id') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($dynasties as $dynasty): ?>
        <tr>
            <td><?= $this->Number->format($dynasty->id) ?></td>
            <td><?= h($dynasty->polity) ?></td>
            <td><?= h($dynasty->dynasty) ?></td>
            <td><?= $this->Number->format($dynasty->sequence) ?></td>
            <td><?= $dynasty->has('provenience') ? $this->Html->link($dynasty->provenience->id, ['controller' => 'Proveniences', 'action' => 'view', $dynasty->provenience->id]) : '' ?></td>
            <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                        ['action' => 'view', $dynasty->id],
                        ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $dynasty->id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $dynasty->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $dynasty->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<div>
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
        <?= $this->Paginator->first() ?>
        <?= $this->Paginator->prev() ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next() ?>
        <?= $this->Paginator->last() ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>

